import com.devcamp.Animal;
import com.devcamp.Cat;
import com.devcamp.Dog;
import com.devcamp.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        
        Animal animal1 = new Animal("Phoenix");
        Animal animal2 = new Animal("Dragon");
        System.out.println(animal1);
        System.out.println(animal2);

        Mammal mammal1 = new Mammal("Cow");
        Mammal mammal2 = new Mammal("Goat");
        System.out.println(mammal1);
        System.out.println(mammal2);

        Cat cat1 = new Cat("Saphia");
        Cat cat2 = new Cat("Niccky");
        cat1.greets();
        cat2.greets();
        System.out.println(cat1);
        System.out.println(cat2);

        Dog dog1 = new Dog("Lulu");
        Dog dog2 = new Dog("Beggy");
        System.out.println(dog1);
        System.out.println(dog2);
        dog1.greets();
        dog2.greets();
        dog1.greets(dog2);
    }
}
