package com.devcamp;

public class Mammal extends Animal {
    
    public Mammal(String name){
        super(name);
    }

    @Override
    public String toString(){
        return "Mamaml [Animal [name= " + name + "]]";
    }
}
